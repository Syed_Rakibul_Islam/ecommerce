<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 30-Nov-16
 * Time: 1:30 PM
 */

function ImageDir($dateTime)
{
    $dateTime = \Carbon\Carbon::parse($dateTime);
    $thisYear = $dateTime->year;
    $thisMonth = $dateTime->month;
    $thisDay = $dateTime->day;
    return ($thisYear . '/' . $thisMonth . '/' . $thisDay);
}
