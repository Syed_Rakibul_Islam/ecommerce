<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 30-Nov-16
 * Time: 1:32 PM
 */

function setSessionMessage($next, $message)
{
    /*
     * ************************
     * Session Factory
     * ************************
     */
    $session_factory = new \Aura\Session\SessionFactory();
    $session = $session_factory->newInstance($_COOKIE);

    $segment = $session->getSegment($next);
    $segment->setFlash('message', $message);
}
function getSessionMessage($path)
{
    /*
     * ************************
     * Session Factory
     * ************************
     */
    $session_factory = new \Aura\Session\SessionFactory();
    $session = $session_factory->newInstance($_COOKIE);

    $segment = $session->getSegment($path);
    $message = $segment->getFlash('message', 'Not Set');
    return $message;
}