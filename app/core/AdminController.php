<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 21-Nov-16
 * Time: 10:18 AM
 */

class AdminController extends Controller
{
    public $auth;
    public $authImgPath;
    public function __construct()
    {
        if(!isset($_SESSION['admin']))
        {
            redirect(root() . '/admin/login/');
        }
        $this->auth = UserModel::findOrFail($_SESSION['admin']);
        if (isset($this->auth->userImage->name))
        {
            $subPath = ImageDir($this->auth->userImage->updated_at);
            $this->authImgPath = '/public/images/users/' . $subPath . '/' . $this->auth->userImage->name;
        }
        else
        {
            $this->authImgPath = '/public/images/users/avatar.jpg';
        }
    }
}