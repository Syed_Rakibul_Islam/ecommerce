<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 22-Dec-16
 * Time: 2:14 PM
 */

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'quantity', 'price', 'shop_id','user_id'];

    public  function productImages(){
        return $this->hasMany('ProductImageModel', 'product_id', 'id');
    }
    public  function shop(){
        return $this->belongsTo('ShopModel', 'shop_id');
    }
    public  function user(){
        return $this->belongsTo('UserModel', 'user_id');
    }
}