<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 22-Dec-16
 * Time: 5:58 PM
 */

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id'];

    public  function productImages(){
        return $this->hasMany('ProductImageModel', 'image_id', 'id');
    }
    public  function user(){
        return $this->belongsTo('UserModel', 'user_id');
    }
}