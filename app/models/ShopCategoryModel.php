<?php

use Illuminate\Database\Eloquent\Model;

class ShopCategoryModel extends Model{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_category';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id'];

    public  function category(){
        return $this->hasMany('CategoryModel', 'category_id', 'category_id');
    }
}