<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 08-Dec-16
 * Time: 6:24 PM
 */

use Illuminate\Database\Eloquent\Model;

class UserImageModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userimage';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'user_id'];

    public  function user(){
        return $this->hasOne('UserModel', 'id');
    }
}