<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 22-Dec-16
 * Time: 6:03 PM
 */

use Illuminate\Database\Eloquent\Model;

class ProductImageModel extends Model{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_image';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = array('product_id', 'image_id');

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'image_id'];


    public  function product(){
        return $this->belongsTo('ProductModel', 'product_id');
    }
    public  function user(){
        return $this->belongsTo('ImageModel', 'image_id');
    }
}