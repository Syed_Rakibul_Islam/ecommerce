<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 11-Dec-16
 * Time: 4:41 PM
 */
class Shop extends UserController
{
    public function  __construct()
    {
        parent::__construct();
        if(!isset($_SESSION['userid']))
        {
            redirect(root() . '/');
        }
    }
    public function index()
    {
        $shop = ShopModel::where('user_id', '=', $_SESSION['userid'])->orderBy('updated_at', 'desc')->first();
        if ($shop)
        {
            $this->view('shop/show', []);
            echo 'asd';
        }
        else
        {
            $this->view('shop/policy', []);

        }
    }

}
