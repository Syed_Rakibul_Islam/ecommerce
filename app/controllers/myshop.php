<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 14-Jan-17
 * Time: 5:29 PM
 */

class MyShop extends UserController
{
    public function index()
    {
        $shop = ShopModel::where('user_id', '=', $_SESSION['userid'])->orderBy('updated_at', 'desc')->first();;
        if ($shop)
        {
            $this->view('shop/policy', []);
        }
        else
        {
            $this->view('shop/policy', []);
        }
    }
    public function create()
    {
        $categories = CategoryModel::all();
        $this->view('shop/create', ['categories' => $categories]);
    }

    public function store()
    {
        if (isset($_POST['createShop'])){
            $shop = ShopModel::create([
                'name' => $_POST['shop'],
                'user_id' => $_SESSION['userid']
            ]);
            if ($shop){
                $shopCategory = ShopCategoryModel::create([
                    'category_id' => $_POST['category']
                ]);
                if($shopCategory){
                    redirect(root() . '/shop/' . $shop->name);
                }
            }
            else {
                redirect(root() . '/myshop/create');
            }
        }
        else {
            redirect(root() . '/myshop/create');
        }
    }
}