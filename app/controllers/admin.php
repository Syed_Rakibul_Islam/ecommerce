<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 02-Nov-16
 * Time: 1:10 PM
 */


class Admin extends Controller
{

    public function index()
    {
        if(!isset($_SESSION['admin']))
        {
            redirect(root() . '/admin/login/');
        }
        else
        {
            redirect(root() . '/admin/home/');
        }
    }
}