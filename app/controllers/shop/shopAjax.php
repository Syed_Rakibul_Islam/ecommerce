<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 20-Dec-16
 * Time: 5:55 PM
 */
use Carbon\Carbon;
use Sirius\Upload\Handler as UploadHandler;

class ShopAjax
{
    public function __construct()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 'xmlhttprequest' == strtolower($_SERVER['HTTP_X_REQUESTED_WITH']))
        {

        }
        else
        {
            redirect(root());
        }
    }
    public function showShop()
    {
        if((isset($_POST['data'])) && ($_POST['data'] != '') )
        {
            $shop = ShopModel::where('name', '=', $_POST['data'])->first();
            echo (isset($shop)) ? 'true' : 'false';
        }
        else
        {
            echo 'error';
        }
    }
    public function storeImage()
    {
        if((isset($_FILES['data'])) && ($_FILES['data'] != '') )
        {
            /*
             * Carbon DateTimestamp
             */
            $thisYear = Carbon::now()->year;
            $thisMonth = Carbon::now()->month;
            $thisDay = Carbon::now()->day;
            $thisTime = Carbon::now()->timestamp;

            /*
             * ******************************
             * Set Image Directory
             * *****************************
             */
            $mainDir = 'public/images/products/images';
            if (!is_dir($mainDir . '/' . $thisYear)) {
                mkdir($mainDir . '/' . $thisYear);
            }
            if (!is_dir($mainDir . '/' . $thisYear . '/' . $thisMonth)) {
                mkdir($mainDir . '/' . $thisYear . '/' . $thisMonth);
            }
            if (!is_dir($mainDir . '/' . $thisYear . '/' . $thisMonth . '/' . $thisDay)) {
                mkdir($mainDir . '/' . $thisYear . '/' . $thisMonth . '/' . $thisDay);
            }

            $uploadHandler = new UploadHandler($mainDir . '/' . $thisYear . '/' . $thisMonth . '/' . $thisDay);

            // validation rules
            $uploadHandler->addRule('extension', ['allowed' => ['jpg', 'jpeg', 'png']], ' should be a valid image (jpg, jpeg, png) and also less than 1MB', 'Profile picture');
            $uploadHandler->addRule('size', ['max' => '1M'], '{label} should have less than {max}', 'Profile picture');
            $uploadHandler->setPrefix($thisTime . '__');

            $result = $uploadHandler->process($_FILES['data']); // ex: subdirectory/my_headshot.png

            if ($result->isValid()) {
                // do something with the image like attaching it to a model etc
                try {
                    $addImage = ImageModel::create([
                        'name' => $result->name,
                        'user_id' => $_SESSION['userid']
                    ]);
                    if (isset($addImage)) {
                        $result->confirm(); // this will remove the .lock file
                        echo 'success';
                    }

                } catch (\Exception $e) {
                    // something wrong happened, we don't need the uploaded files anymore
                    $result->clear();
                    throw $e;
                }
            } else {
                echo 'error';
            }
        }
        else
        {
            echo 'invalid';
        }

    }
}