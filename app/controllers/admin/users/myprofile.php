<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 21-Nov-16
 * Time: 10:15 AM
 */

use Carbon\Carbon;
use Sirius\Upload\Handler as UploadHandler;
use Sirius\Validation\RuleFactory;
use Sirius\Validation\ErrorMessage;
use Sirius\Validation\Validator;

class MyProfile extends AdminController
{

    public function index()
    {
        $user = UserModel::find($_SESSION['userid']);
        if (isset($user->userImage->name))
        {
            $subPath = ImageDir($user->userImage->updated_at);
            $imgPath = '/public/images/users/' . $subPath . '/' . $user->userImage->name;
        }
        else
        {
            $imgPath = '/public/images/users/avatar.jpg';
        }

        /*
         * ************************
         * Session Factory
         * ************************
         */
        $message = getSessionMessage('admin/users/myprofile/index');

        /*
         * ************************
         * View
         * ************************
         */
        $this->view('admin/users/myprofile/index', ['message' => $message, 'user' => $user, 'imgPath' => $imgPath]);
    }
    public function edit_image()
    {
        /*
         * ************************
         * Session Factory
         * ************************
         */
        $message = getSessionMessage('admin/users/myprofile/edit_image');

        /*
         * ************************
         * View
         * ************************
         */
        $this->view('admin/users/myprofile/edit_image', ['message' => $message]);
    }
    public function store_image()
    {
        if (isset($_FILES['image']))
        {
            /*
             * Carbon DateTimestamp
             */
            $thiYear = Carbon::now()->year;
            $thiMonth = Carbon::now()->month;
            $thiDay = Carbon::now()->day;
            $thisTime = Carbon::now()->timestamp;

            /*
             * ******************************
             * Set Image Directory
             * *****************************
             */
            $mainDir = 'public/images/users';
            if (!is_dir($mainDir . '/' . $thiYear))
            {
                mkdir($mainDir . '/' . $thiYear);
            }
            if (!is_dir($mainDir . '/' . $thiYear . '/' . $thiMonth))
            {
                mkdir($mainDir . '/' . $thiYear . '/' . $thiMonth);
            }
            if (!is_dir($mainDir . '/' . $thiYear . '/' . $thiMonth . '/' . $thiDay))
            {
                mkdir($mainDir . '/' . $thiYear . '/' . $thiMonth . '/' . $thiDay);
            }

            $uploadHandler = new UploadHandler($mainDir . '/' . $thiYear . '/' . $thiMonth . '/' . $thiDay);

            // validation rules
            $uploadHandler->addRule('extension', ['allowed' => ['jpg', 'jpeg', 'png']], ' should be a valid image (jpg, jpeg, png) and also less than 1MB', 'Profile picture');
            $uploadHandler->addRule('size', ['max' => '1M'], '{label} should have less than {max}', 'Profile picture');
            $uploadHandler->setPrefix($thisTime . '__');


            $userImage = UserImageModel::find($_SESSION['userid']);
            if (isset($userImage->name))
            {
                $result = $uploadHandler->process($_FILES['image']); // ex: subdirectory/my_headshot.png

                if ($result->isValid()) {
                    // do something with the image like attaching it to a model etc
                    try {
                        $subPath = ImageDir($userImage->updated_at);
                        $imgPath = 'public/images/users/' . $subPath . '/' . $userImage->name;
                        if(unlink($imgPath)) {
                            $setImage = $userImage->update(array(
                                'name' => $result->name,
                                'user_id' => $_SESSION['userid']
                            ));
                            if (isset($setImage)) {
                                $result->confirm(); // this will remove the .lock file
                                /*
                                 * ******************************
                                 * Flash Session
                                 * *****************************
                                 */
                                $sessionMessage = [
                                    'type' => 'success',
                                    'title' => 'Success',
                                    'message' => 'Profile picture updated!!'];
                                setSessionMessage('admin/users/myprofile/index', $sessionMessage);

                                /*
                                 * ******************************
                                 * Redirect URL
                                 * *****************************
                                 */
                                redirect(root() . '/admin/users/myprofile/');
                            }
                            else
                            {
                                /*
                                 * ******************************
                                 * Flash Session
                                 * *****************************
                                 */
                                $sessionMessage = [
                                    'type' => 'danger',
                                    'title' => 'Error',
                                    'message' => 'Something missing!! Please try again....'];
                                setSessionMessage('admin/users/myprofile/edit_image', $sessionMessage);

                                /*
                                 * ******************************
                                 * Redirect URL
                                 * *****************************
                                 */
                                redirect(root() . '/admin/users/myprofile/edit_image');
                            }
                        }
                        else
                        {
                            /*
                             * ******************************
                             * Flash Session
                             * *****************************
                             */

                            $sessionMessage = [
                                'type' => 'danger',
                                'title' => 'Error',
                                'message' => 'Old image directory missing.'];
                            setSessionMessage('admin/users/myprofile/edit_image', $sessionMessage);

                            /*
                             * ******************************
                             * Redirect URL
                             * *****************************
                             */
                            redirect(root() . '/admin/users/myprofile/edit_image');
                        }
                    } catch (\Exception $e) {
                        // something wrong happened, we don't need the uploaded files anymore
                        $result->clear();
                        throw $e;
                    }
                }
                else
                {
                    // image was not moved to the container, where are error messages

                    /*
                     * ******************************
                     * Flash Session
                     * *****************************
                     */
                    $sessionMessage = [
                        'type' => 'danger',
                        'title' => 'Error',
                        'message' => $result->getMessages()[0]->template];
                    setSessionMessage('admin/users/myprofile/edit_image', $sessionMessage);

                    /*
                     * ******************************
                     * Redirect URL
                     * *****************************
                     */
                    redirect(root() . '/admin/users/myprofile/edit_image');
                }
            }
            else
            {
                $result = $uploadHandler->process($_FILES['image']); // ex: subdirectory/my_headshot.png

                if ($result->isValid()) {
                    // do something with the image like attaching it to a model etc
                    try {
                        $setImage = UserImageModel::create([
                            'id' => $_SESSION['userid'],
                            'name' => $result->name,
                            'user_id' => $_SESSION['userid']
                        ]);
                        if (isset($setImage))
                        {
                            $result->confirm(); // this will remove the .lock file

                            /*
                             * ******************************
                             * Flash Session
                             * *****************************
                             */
                            $sessionMessage = [
                                'type' => 'success',
                                'title' => 'Success',
                                'message' => 'Profile picture added!!'];
                            setSessionMessage('admin/users/myprofile/index', $sessionMessage);

                            /*
                             * ******************************
                             * Redirect URL
                             * *****************************
                             */
                            redirect(root() . '/admin/users/myprofile/');
                        }

                    } catch (\Exception $e) {
                        // something wrong happened, we don't need the uploaded files anymore
                        $result->clear();
                        throw $e;
                    }
                }
                else
                {
                    // image was not moved to the container, where are error messages

                    /*
                     * ******************************
                     * Flash Session
                     * *****************************
                     */
                    $sessionMessage = [
                        'type' => 'danger',
                        'title' => 'Error',
                        'message' => $result->getMessages()[0]->template];
                    setSessionMessage('admin/users/myprofile/edit_image', $sessionMessage);

                    /*
                     * ******************************
                     * Redirect URL
                     * *****************************
                     */
                    redirect(root() . 'admin/users/myprofile/edit_image');
                }
            }
        }
        else
        {
            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . 'admin/users/myprofile');
        }

    }
    public function edit_profile()
    {
        $user = UserModel::find($_SESSION['userid']);
        $this->view('admin/users/myprofile/edit_profile', ['user' => $user]);
    }
    public function store_profile()
    {
        if (isset($_POST['submit']))
        {
            $name = $_POST['name'];
            $birth = $_POST['birth'];
            $gender = '';
            if(isset($_POST['gender']))
            {
                $gender = $_POST['gender'];
            }
            $email = $_POST['email'];
            $address = $_POST['address'];
            $contact = $_POST['contact'];
            $profession = $_POST['profession'];
            $company = $_POST['company'];
            $designation = $_POST['designation'];
            $about = $_POST['about'];
            $website = $_POST['website'];
            $facebook = $_POST['facebook'];
            $twitter = $_POST['twitter'];
            $googleplus = $_POST['googleplus'];
            $linkedin = $_POST['linkedin'];
            $github = $_POST['github'];

            $ruleFactory = new RuleFactory;
            $errorMessagePrototype = new ErrorMessage;
            $validator = new Validator($ruleFactory, $errorMessagePrototype);
            $validator->add(array(
                'name:Name' => 'required | minlength(min=4) | maxlength(100)({label} must have less than {max} characters)',
                'birth:Birth Date' => 'Date',
                'email:Email' => 'required | Email',
                'address:Address' => 'minlength(min=10) | maxlength(255)',
                'contact:Contact' => 'minlength(min=5) | maxlength(50)',
                'profession:Profession' => 'minlength(min=5) | maxlength(50)',
                'company:Company' => 'minlength(min=5) | maxlength(100)',
                'designation:Designation' => 'minlength(min=5) | maxlength(50)',
                'about:About me' => 'minlength(min=10) | maxlength(255)',
                'website:Website' => 'website | maxlength(255)',
                'facebook:Facebook' => 'website | maxlength(255)',
                'twitter:Twitter' => 'website | maxlength(255)',
                'googleplus:Googleplus' => 'website | maxlength(255)',
                'linkedin:Linkedin' => 'website | maxlength(255)',
                'github:Github' => 'website | maxlength(255)'
            ));

            if ($validator->validate($_POST))
            {
                $userProfile = UserProfileModel::find($_SESSION['userid']);
                if(isset($userProfile->id))
                {
                    $userProfile->user->update(array(
                        'name' => $name,
                        'email' => $email,
                        'contact' => $contact
                    ));
                    $updateProfile = $userProfile->update(array(
                        'birth_date' => $birth,
                        'gender' => $gender,
                        'address' => $address,
                        'profession' => $profession,
                        'company_name' => $company,
                        'designation' => $designation,
                        'website' => $website,
                        'about' => $about,
                        'fb' => $facebook,
                        'tw' => $twitter,
                        'gplus' => $googleplus,
                        'ln' => $linkedin,
                        'git' => $github,
                        'user_id' => $_SESSION['userid']
                    ));
                    if(isset($updateProfile))
                    {
                        /*
                         * ******************************
                         * Flash Session
                         * *****************************
                         */
                        $sessionMessage = [
                            'type' => 'success',
                            'title' => 'Success',
                            'message' => 'Profile picture updated!!'];
                        setSessionMessage('admin/users/myprofile/index', $sessionMessage);
                        /*
                         * ******************************
                         * Redirect URL
                         * *****************************
                         */
                        redirect(root() . '/admin/users/myprofile/');
                    }
                    else
                    {
                        echo 'error';
                    }
                }
                else
                {
                    $user = UserModel::find($_SESSION['userid']);
                    $user->update(array(
                        'name' => $name,
                        'email' => $email,
                        'contact' => $contact
                    ));
                    $storeProfile = UserProfileModel::create([
                        'id' => $_SESSION['userid'],
                        'birth_date' => $birth,
                        'gender' => $gender,
                        'address' => $address,
                        'profession' => $profession,
                        'company_name' => $company,
                        'designation' => $designation,
                        'website' => $website,
                        'about' => $about,
                        'fb' => $facebook,
                        'tw' => $twitter,
                        'gplus' => $googleplus,
                        'ln' => $linkedin,
                        'git' => $github,
                        'user_id' => $_SESSION['userid']
                    ]);
                    if(isset($storeProfile->id))
                    {
                        /*
                         * ******************************
                         * Flash Session
                         * *****************************
                         */
                        $sessionMessage = [
                            'type' => 'success',
                            'title' => 'Success',
                            'message' => 'Profile picture updated!!'];
                        setSessionMessage('admin/users/myprofile/index', $sessionMessage);
                        /*
                         * ******************************
                         * Redirect URL
                         * *****************************
                         */
                        redirect(root() . '/admin/users/myprofile/');
                    }
                    else
                    {
                        echo 'Error';
                    }
                }
            }
        }
        else
        {
            $this->view('admin/errors/404');
        }
    }
}