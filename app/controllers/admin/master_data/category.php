<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 11-Nov-16
 * Time: 7:05 PM
 */

class Category extends AdminController
{

    public function index()
    {
        $category = CategoryModel::all();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category');

        $this->view('admin/master_data/category/index', ['message' => $message, 'category' => $category]);
    }
    public function create()
    {
        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/create');

        $this->view('admin/master_data/category/create', ['message' => $message]);
    }
    public function store()
    {
        if (isset($_POST['submit']))
        {
                $category = CategoryModel::create([
                    'name' => $_POST['name'],
                    'user_id' => $_POST['user_id']
                ]);
                if ($category)
                {
                    /*
                     * ******************************
                     * SET Flash Session
                     * *****************************
                     */
                    $sessionMessage = [
                        'type' => 'success',
                        'title' => 'Success',
                        'message' => 'Category name "'. $_POST['name'] .'" added!!'];
                    setSessionMessage('admin/master_data/category', $sessionMessage);

                    /*
                     * ******************************
                     * Redirect URL
                     * *****************************
                     */
                    redirect(root() . '/admin/master_data/category/');
                }
                else
                {
                    /*
                     * ******************************
                     * SET Flash Session
                     * *****************************
                     */
                    $sessionMessage = [
                        'type' => 'danger',
                        'title' => 'Error',
                        'message' => 'Something missing. Please try again'];
                    setSessionMessage('admin/master_data/category/create', $sessionMessage);

                    /*
                     * ******************************
                     * Redirect URL
                     * *****************************
                     */
                    redirect(root() . '/admin/master_data/category/create');
                }
        }
        else
        {
            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/create');
        }
    }
    public function edit($id = '')
    {
        $category = CategoryModel::find($id);
        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/edit');

        if (isset($category))
        {
            $this->view('admin/master_data/category/edit', ['message' => $message, 'category' => $category]);
        }
        else
        {
            $this->view('admin/errors/404');
        }
    }
    public function update($id = '')
    {
        $category = CategoryModel::find($id);
        if (isset($category))
        {
            if (isset($_POST['submit']))
            {
                $category->update(array(
                    'name' => $_POST['name'],
                    'user_id' => $_POST['user_id']
                ));
                /*
                 * ******************************
                 * Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category name "'. $_POST['name'] .'" updated!!'];
                setSessionMessage('admin/master_data/category', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/');
            }
            else
            {
                /*
                * ******************************
                * Redirect URL
                * *****************************
                */
                redirect(root() . '/admin/master_data/category/edit');
            }
        }
        else
        {

            /*
             * ******************************
             * SET Flash Session
             * *****************************
             */
            $sessionMessage = [
                'type' => 'danger',
                'title' => 'Error',
                'message' => 'Something missing. Please try again'];
            setSessionMessage('admin/master_data/category/edit', $sessionMessage);

            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/edit');
        }
    }
    public function destroy($id = '')
    {
        try {
            $category = CategoryModel::find($id);
            $category->delete();
        }
        catch (Exception $e){
            echo 'error';
        }
    }
}