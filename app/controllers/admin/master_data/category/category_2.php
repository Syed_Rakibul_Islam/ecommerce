<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 15-Nov-16
 * Time: 5:27 PM
 */



class Category_2 extends AdminController
{
    public function index()
    {
        $category2 = Category2Model::all();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2');


        $this->view('admin/master_data/category/category_2/index', ['message' => $message, 'category2' => $category2]);
    }
    public function create()
    {
        $categories = CategoryModel::orderBy('name')->get();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/create');

        $this->view('admin/master_data/category/category_2/create', ['message' => $message, 'categories' => $categories]);
    }
    public function store()
    {
        if (isset($_POST['submit']))
        {
            $category2 = Category2Model::create([
                'name' => $_POST['name'],
                'category_id' => $_POST['category_id'],
                'user_id' => $_POST['user_id']
            ]);
            if ($category2)
            {
                /*
                 * ******************************
                 * Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 2 name "'. $_POST['name'] .'" added!!'];
                setSessionMessage('admin/master_data/category/category_2', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/');
            }
            else{
                /*
                 * ******************************
                 * SET Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'danger',
                    'title' => 'Error',
                    'message' => 'Something missing. Please try again'];
                setSessionMessage('admin/master_data/category/category_2/create', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/create');
            }
        }
        else
        {
            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/category_2/create');
        }
    }
    public function edit($id = '')
    {
        $categories = CategoryModel::all();
        $category2 = Category2Model::find($id);

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/edit');
        if (isset($category2))
        {
            $this->view('admin/master_data/category/category_2/edit', ['message' => $message, 'category2' => $category2, 'categories' => $categories]);
        }
        else
        {
            $this->view('admin/errors/404');
        }
    }
    public function update($id = '')
    {
        $category2 = Category2Model::find($id);
        if (isset($category2))
        {
            if (isset($_POST['submit']))
            {
                $category2->update(array('name' => $_POST['name'], 'category_id' => $_POST['category_id'], 'user_id' => $_POST['user_id']));
                /*
                 * ******************************
                 * Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 2 name "'. $_POST['name'] .'" updated!!'];
                setSessionMessage('admin/master_data/category/category_2', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2');
            }
            else
            {
                /*
                * ******************************
                * Redirect URL
                * *****************************
                */
                redirect(root() . '/admin/master_data/category/category_2/edit');
            }
        }
        else
        {
            /*
             * ******************************
             * SET Flash Session
             * *****************************
             */
            $sessionMessage = [
                'type' => 'danger',
                'title' => 'Error',
                'message' => 'Something missing. Please try again'];
            setSessionMessage('admin/master_data/category/category_2/edit', $sessionMessage);

            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/category_2/edit');
        }
    }
    public function destroy($id = '')
    {
        try {
            $category2 = Category2Model::find($id);
            $category2->delete();
        }
        catch (Exception $e){
            echo 'error';
        }
    }
}