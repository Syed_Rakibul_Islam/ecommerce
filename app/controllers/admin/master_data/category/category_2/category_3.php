<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 15-Nov-16
 * Time: 7:20 PM
 */

class Category_3 extends AdminController
{
    public function index()
    {
        $category3 = Category3Model::all();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3');

        $this->view('admin/master_data/category/category_2/category_3/index', ['message' => $message, 'category3' => $category3]);
    }
    public function create()
    {
        $categories2 = Category2Model::orderBy('name')->get();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3/create');

        $this->view('admin/master_data/category/category_2/category_3/create', ['message' => $message, 'categories2' => $categories2]);
    }
    public function store()
    {
        if (isset($_POST['submit']))
        {
            $category3 = Category3Model::create([
                'name' => $_POST['name'],
                'category_2_id' => $_POST['category_2_id'],
                'user_id' => $_POST['user_id']
            ]);
            if ($category3)
            {
                /*
                 * ******************************
                 * Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 3 name "'. $_POST['name'] .'" added!!'];
                setSessionMessage('admin/master_data/category/category_2/category_3', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3');
            }
            else{
                /*
                 * ******************************
                 * SET Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'danger',
                    'title' => 'Error',
                    'message' => 'Something missing. Please try again'];
                setSessionMessage('admin/master_data/category/category_2/category_3/create', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3/create');
            }
        }
        else
        {
            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/category_2/category_3/create');
        }
    }
    public function edit($id = '')
    {
        $categories2 = Category2Model::all();
        $category3 = Category3Model::find($id);

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3/edit');

        if (isset($category3))
        {
            $this->view('admin/master_data/category/category_2/category_3/edit', ['message' => $message, 'category3' => $category3, 'categories2' => $categories2]);
        }
        else
        {
            $this->view('admin/errors/404');
        }
    }
    public function update($id = '')
    {
        $category3 = Category3Model::find($id);
        if (isset($category3))
        {
            if (isset($_POST['submit']))
            {
                $category3->update(array('name' => $_POST['name'], 'category_2_id' => $_POST['category_2_id'], 'user_id' => $_POST['user_id']));

                /*
                 * ******************************
                 * Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 3 name "'. $_POST['name'] .'" updated!!'];
                setSessionMessage('admin/master_data/category/category_2/category_3', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3');
            }
            else
            {
                /*
                * ******************************
                * Redirect URL
                * *****************************
                */
                redirect(root() . '/admin/master_data/category/category_2/category_3/edit');
            }
        }
        else
        {
            /*
              * ******************************
              * SET Flash Session
              * *****************************
              */
            $sessionMessage = [
                'type' => 'danger',
                'title' => 'Error',
                'message' => 'Something missing. Please try again'];
            setSessionMessage('admin/master_data/category/category_2/category_3/edit', $sessionMessage);

            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/category_2/category_3/edit');
        }
    }
    public function destroy($id = '')
    {
        try {
            $category3 = Category3Model::find($id);
            $category3->delete();
        }
        catch (Exception $e){
            echo 'error';
        }
    }
}