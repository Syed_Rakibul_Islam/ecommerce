<?php
/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 15-Nov-16
 * Time: 7:51 PM
 */

class Category_4 extends AdminController
{
    public function index()
    {
        $category4 = Category4Model::all();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3/category_4');

        $this->view('admin/master_data/category/category_2/category_3/category_4/index', ['message' => $message, 'category4' => $category4]);
    }
    public function create()
    {
        $categories3 = Category3Model::orderBy('name')->get();

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3/category_4/create');

        $this->view('admin/master_data/category/category_2/category_3/category_4/create', ['message' => $message, 'categories3' => $categories3]);
    }
    public function store()
    {
        if (isset($_POST['submit']))
        {
            $category4 = Category4Model::create([
                'name' => $_POST['name'],
                'category_3_id' => $_POST['category_3_id'],
                'user_id' => $_POST['user_id']
            ]);
            if ($category4)
            {
                /*
                * ******************************
                * Flash Session
                * *****************************
                */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 4 name "'. $_POST['name'] .'" added!!'];
                setSessionMessage('admin/master_data/category/category_2/category_3/category_4', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3/category_4');
            }
            else{
                /*
                 * ******************************
                 * SET Flash Session
                 * *****************************
                 */
                $sessionMessage = [
                    'type' => 'danger',
                    'title' => 'Error',
                    'message' => 'Something missing. Please try again'];
                setSessionMessage('admin/master_data/category/category_2/category_3/category_4/create', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3/category_4/create');
            }
        }
        else
        {
            /*
            * ******************************
            * Redirect URL
            * *****************************
            */
            redirect(root() . '/admin/master_data/category/category_2/category_3/category_4/create');
        }
    }
    public function edit($id = '')
    {
        $categories3 = Category3Model::all();
        $category4 = Category4Model::find($id);

        /*
         * ************************
         * GET Flash Session
         * ************************
         */
        $message = getSessionMessage('admin/master_data/category/category_2/category_3/category_4/edit');

        if (isset($category4))
        {
            $this->view('admin/master_data/category/category_2/category_3/category_4/edit', ['message' => $message, 'category4' => $category4, 'categories3' => $categories3]);
        }
        else
        {
            $this->view('admin/errors/404');
        }
    }
    public function update($id = '')
    {
        $category4 = Category4Model::find($id);
        if (isset($category4))
        {
            if (isset($_POST['submit']))
            {
                $category4->update(array('name' => $_POST['name'], 'category_3_id' => $_POST['category_3_id'], 'user_id' => $_POST['user_id']));

                /*
                * ******************************
                * Flash Session
                * *****************************
                */
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Success',
                    'message' => 'Category 4 name "'. $_POST['name'] .'" updated!!'];
                setSessionMessage('admin/master_data/category/category_2/category_3/category_4', $sessionMessage);

                /*
                 * ******************************
                 * Redirect URL
                 * *****************************
                 */
                redirect(root() . '/admin/master_data/category/category_2/category_3/category_4');
            }
            else
            {
                /*
                * ******************************
                * Redirect URL
                * *****************************
                */
                redirect(root() . '/admin/master_data/category/category_2/category_3/category_4/edit');
            }
        }
        else
        {
            /*
             * ******************************
             * SET Flash Session
             * *****************************
             */
            $sessionMessage = [
                'type' => 'danger',
                'title' => 'Error',
                'message' => 'Something missing. Please try again'];
            setSessionMessage('admin/master_data/category/category_2/category_3/category_4/edit', $sessionMessage);

            /*
             * ******************************
             * Redirect URL
             * *****************************
             */
            redirect(root() . '/admin/master_data/category/category_2/category_3/category_4/edit');
        }
    }
    public function destroy($id = '')
    {
        try {
            $category4 = Category4Model::find($id);
            $category4->delete();
        }
        catch (Exception $e){
            echo 'error';
        }
    }
}