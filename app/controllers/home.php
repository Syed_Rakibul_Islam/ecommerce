<?php
/**
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 01-Nov-16
 * Time: 6:23 PM
 */

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class Home extends UserController
{

    public function index($name = '')
    {
        /*
         * ************************
         * Session Factory
         * ************************
         */
        $message = getSessionMessage('home');
        if(isset($_SESSION['userid']))
        {
            $this->view('register/index', ['message' => $message,]);
        }
        else{
            $this->view('unregister/index', ['message' => $message,]);
        }


    }
    public function login()
    {

        if(isset($_POST['login']))
        {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $md5password = md5($password);

            $user = UserModel::where('username', $username)
                ->where('password', $md5password)
                ->first();
            if (!empty($user))
            {
                $_SESSION['userid'] = $user->id;
                $_SESSION['name'] = $user->name;
                $_SESSION['username'] = $user->username;

                $admin = AdminModel::where('id', $user->id)->first();
                if (!empty($admin))
                {
                    $_SESSION['admin'] = $admin->id;

                    $super = SuperModel::where('id', $admin->id)->first();

                    if (!empty($super))
                    {
                        $_SESSION['superuserid'] = $super->id;
                    }
                }
                $sessionMessage = [
                    'type' => 'success',
                    'title' => 'Login',
                    'message' => 'Login successfully!!'];
                setSessionMessage('home', $sessionMessage);
            }
            else
            {
                $sessionMessage = [
                    'type' => 'danger',
                    'title' => 'Login',
                    'message' => 'Username and password does\'t match!!'];
                setSessionMessage('home', $sessionMessage);
            }
        }

        redirect(root() . '/');

    }
    public function logout()
    {
        session_destroy();

        $sessionMessage = [
            'type' => 'warning',
            'title' => 'Logout',
            'message' => 'Logout successfully!!'];
        setSessionMessage('home', $sessionMessage);


        redirect(root() . '/');
    }
    public function register()
    {
        if (isset($_POST['register']))
        {
            $user = new stdClass();
            $user->name = $_POST['name'];
            $user->username = $_POST['username'];
            $user->email = $_POST['email'];
            $user->contact = $_POST['contact'];
            $user->password = $_POST['password'];

            $userValidator = v::attribute('name', v::stringType()->length(5, 100))
                ->attribute('username', v::stringType()->length(4, 50))
                ->attribute('email', v::email())
                ->attribute('contact', v::stringType()->length(5, 100))
                ->attribute('password', v::stringType()->length(5, 100));
            try
            {
                $userValidator->assert($user);

                $user = UserModel::create([
                    'name' => $user->name,
                    'username' => $user->username,
                    'email' => $user->email,
                    'contact' => $user->contact,
                    'password' => md5($user->password)
                ]);
                if (isset($user))
                {
                    $sessionMessage = [
                        'type' => 'success',
                        'title' => 'Register success',
                        'message' => 'Register succeed!!'];
                    setSessionMessage('home', $sessionMessage);
                }
                else {
                    $sessionMessage = [
                        'type' => 'danger',
                        'title' => 'Register Error',
                        'message' => 'Something missing... Try again!!'];
                    setSessionMessage('home', $sessionMessage);
                }
            }
            catch(NestedValidationException $exception)
            {
                $sessionMessage = [
                    'type' => 'danger',
                    'title' => 'Register Error',
                    'message' => $exception->getMessages()];
                setSessionMessage('home', $sessionMessage);
            }
        }
        redirect(root() . '/');
    }



    public function single()
    {

        $this->view('home/single', []);
    }
    public function super()
    {
        $msg = '';

        $isAdmin = UserModel::where('username', '=', 'Admin')->first();
        if(!isset($isAdmin->id)) {
            $user = UserModel::create([
                'name' => 'Super Admin',
                'username' => 'Admin',
                'email' => 'default@mail.com',
                'password' => md5(123456),
            ]);
            if (isset($user->id)) {
                $admin = AdminModel::create([
                    'id' => $user->id,
                    'user_id' => $user->id
                ]);

                if (isset($admin->id)) {
                    SuperModel::create([
                        'id' => $user->id,
                        'user_id' => $user->id
                    ]);
                }
            }

            echo 'success';
        }
        else {
            echo 'Already exists';
        }

    }
    public function updateDB(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "ecommerce_db";

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e)
        {
            echo $e;
        }


        foreach(glob('app/database/tables/*.sql') as $filename)
        {
            try {
                $myfile = fopen($filename, "r") or die("Unable to open file!");
                $sql = fread($myfile, filesize($filename));
                $conn->exec($sql);
                fclose($myfile);
            }
            catch(Exception $e){
                echo 'Error: ' . $e;
            }
        }
        $conn = null;
    }


    public function test(){
        $images = ProductImageModel::all();
        echo $images;
    }
}