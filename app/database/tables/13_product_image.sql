/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 22-Dec-16
 * Time: 2:24 PM
 */
 /*
  **************************************
  * Create Table """"product_image""""
  **************************************
  */
  
CREATE TABLE IF NOT EXISTS `product_image`(
    `product_id` INT NOT NULL,
    `image_id` INT NOT NULL,
    PRIMARY KEY (`product_id`, `image_id`),
    `created_at` TIMESTAMP NULL,
    `updated_at` TIMESTAMP NULL,
    FOREIGN KEY (`product_id`)
      REFERENCES `products`(`id`)
      ON UPDATE CASCADE
      ON DELETE RESTRICT,
      FOREIGN KEY (`image_id`)
      REFERENCES `images`(`id`)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
)