/*
 * Created by PhpStorm.
 * User: Romeo
 * Full Name: Syed Rakibul Islam
 * Email: romeomyname@gmail.com
 * Contact: +880-1737094969
 * Date: 14-Jan-17
 * Time: 5:21 PM
 */
 /*
  **************************************
  * Create Table """"shop_category""""
  **************************************
  */
  
CREATE TABLE IF NOT EXISTS `shop_category`(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `category_id` INT,
    `created_at` TIMESTAMP NULL,
    `updated_at` TIMESTAMP NULL,
    FOREIGN KEY (`category_id`)
      REFERENCES `category`(`id`)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
)