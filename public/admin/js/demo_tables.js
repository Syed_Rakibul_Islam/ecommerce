
    function delete_row(row, urlLink){
        
        var box = $("#mb-remove-row");
        box.addClass("open");
        
        box.find(".mb-control-yes").on("click",function(){
            box.removeClass("open");
            $.ajax({url: urlLink, success: function(result){
                if (result == 'error'){
                    $('#warning-message').html("Unable to remove this data. First you remove its child row then remove it");
                    $('#message-box-warning').show();
                }
                else {
                    $("#"+row).hide("slow",function(){
                        $(this).remove();
                    });
                }
            }});
        });
        
    }
