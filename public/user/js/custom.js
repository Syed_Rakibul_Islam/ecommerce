/**
 * Created by PACER 6 on 09-Dec-16.
 */

// Register password Reveal

$(".reveal").mousedown(function() {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'text'));
})
.mouseup(function() {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
})
.mouseout(function() {
    $(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
});


function goBack() {
    window.history.back();
}




$("#session-alert").fadeTo(10000, 500).slideUp(500, function(){
    $("#session-alert").alert('close');
});



function ajax_request(ajaxData, ajaxUrl){
    var formData = new FormData();
    formData.append('data', ajaxData);
    var tmp = null;
    $.ajax({
        'async': false,
        method: "POST",
        dataType: 'html',
        url: ajaxUrl,
        data: formData,
        contentType: false,
        processData: false
    }).done(function( msg ) {
        tmp = ( msg );
    }).fail(function(msg) {
        tmp = ( "error : " + msg );
    });
    return tmp;
}
