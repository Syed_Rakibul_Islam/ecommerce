/**
 * Created by PACER 6 on 20-Dec-16.
 */


function checkShop(name, url, id)
{
    var getAjaxData = ajax_request(name, url);
    parentDiv = $('#' + id).parent();
    grandParentDiv = $(parentDiv).parent();

    setTimeout(function () {
        if (getAjaxData && getAjaxData == 'true') {
            $('#' + id).removeAttr("style");
            $error = errors = $('<ul/>')
                .addClass('list-unstyled')
                .append('<li>This shop name already exists</li>');

            $(parentDiv).addClass('has-error has-danger');
            $(grandParentDiv).addClass('has-error has-danger');

            $(parentDiv).children(".help-block").empty().append($error);
        }
        else if (getAjaxData && getAjaxData == 'false') {
            $('#' + id).css('border-color', 'rgb(106,175,106)');
            $error = errors = $('<ul/>')
                .addClass('list-unstyled')
                .css({'color': 'rgb(106,175,106)'})
                .append('<li>Valid shop name</li>');

            $(parentDiv).removeClass('has-error has-danger');
            $(grandParentDiv).removeClass('has-error has-danger');

            $(parentDiv).children(".help-block").empty().append($error);
        }
        else {
            $('#' + id).removeAttr("style");
            $error = errors = $('<ul/>')
                .addClass('list-unstyled')
                .append('<li>Shop name missing!!</li>');

            $(parentDiv).addClass('has-error has-danger');
            $(grandParentDiv).addClass('has-error has-danger');

            $(parentDiv).children(".help-block").empty().append($error);
        }
    }, 600);
}

function checkValid(name, id) {
    setTimeout(function () {
        if (name && name != '') {
            $('#' + id).css('border-color', 'rgb(106,175,106)');
        }
        else {
            $('#' + id).removeAttr("style");
        }
    }, 600);
}

function imageUpload(value, url, id) {
    var getAjaxData = ajax_request(value, url);
    if (getAjaxData && getAjaxData == 'success')
    {
        $.notify("Access granted", "success");
    }
    else if (getAjaxData && getAjaxData == 'invalid')
    {
        $.notify("BOOM!", "error");
    }
    else
    {
        $.notify("BOOM!", "error");
    }
}

function tempAlert(msg,duration, type)
{
    var background;
    switch (type) {
        case 'success':
            background = "rgba(76,175,80, 0.8)";
            break;
        case 'error':
            background = "rgba(244,67,54, 0.8)";
            break;
        case 'info':
            background = "rgba(33,150,243, 0.8)";
            break;
        case 'warning':
            background = "rgba(255,152,0, 0.8)";
            break;
        default:
            background = "rgba(25,15,0, 0.8)";
    }
    var el = document.createElement("div");
    el.setAttribute("style","" +
        "position:fixed;" +
        "width:20%;" +
        "top:15%;" +
        "left:80%;" +
        "border-radius:50px;" +
        "padding: 10px;" +
        "background-color: " + background + ";" +
        "color: white;" +
        "transition: opacity 0.6s;");
    var close = document.createElement("span");
    close.innerHTML = '&times;';
    close.setAttribute("onclick", "closeAlert(this)");
    close.setAttribute("style","" +
        "margin-left: 15px;" +
        "color: white;" +
        "font-weight: bold;" +
        "float: right;" +
        "font-size: 22px;" +
        "line-height: 20px;" +
        "cursor: pointer;" +
        "transition: 0.3s;");

    el.innerHTML = msg;
    el.appendChild(close);
    setTimeout(function(){
        el.parentNode.removeChild(el);
    },duration);
    document.body.appendChild(el);
}
function closeAlert(span)
{
    var div = span.parentElement;
    setTimeout(function(){ div.style.display = "none"; }, 600);
}

