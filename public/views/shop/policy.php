<!--  START TOP OF THE PAGE -->
<?php require_once 'public/views/common/top.php';?>
<!-- END TOP OF THE PAGE -->

<!-- WRAPPER START -->
<div class="wrapper">

    <!--   HEADER START   -->
    <?php require_once 'public/views/common/header.php';?>
    <!--   END HEADER   -->
    <!-- HEADING-BANNER START -->
    <?php require_once 'public/views/common/bannerwithbreadcrumbs.php';?>
    <!-- HEADING-BANNER END -->
    <!-- PRODUCT-AREA START -->
    <div class="product-area single-pro-area pt-80 pb-80 product-style-2">
        <div class="container">
            <!-- single-product-tab start -->
            <div class="single-pro-tab">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="single-pro-tab-menu">
                            <!-- Nav tabs -->
                            <ul class="">
                                <li class="active"><a href="#shop" data-toggle="tab">Shop</a></li>
                                <li><a href="#description" data-toggle="tab">Description</a></li>
                                <li ><a href="#policy"  data-toggle="tab">Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="shop">
                                <div class="pro-tab-info pro-description">
                                    <h3 class="tab-title title-border mb-30">Shop</h3>
                                    <p>You don't have any shop. First <a href="<?=root()?>/shop/create">create a shop</a> or read our policy.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="description">
                                <div class="pro-tab-info pro-description">
                                    <h3 class="tab-title title-border mb-30">Description</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="policy">
                                <div class="pro-tab-info pro-description">
                                    <h3 class="tab-title title-border mb-30">Policy</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at est bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- single-product-tab end -->
            <div class="pull-right">
                <a href="<?=root()?>/myshop/create" data-text="Create a shop" class="button-one submit-button btn-bg-3 mt-20">Create a shop</a>
            </div>
        </div>
    </div>
    <!-- PRODUCT-AREA END -->
    <!--  START FOOTER -->
    <?php require_once 'public/views/common/footer.php';?>
    <!-- END FOOTER -->

</div>
<!-- WRAPPER END -->


<!--  START BOTTOM OF THE PAGE -->
<?php require_once 'public/views/common/bottom.php';?>
<!-- END BOTTOM OF THE PAGE -->