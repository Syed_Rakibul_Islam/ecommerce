<!--  START TOP OF THE PAGE -->
<?php require_once 'public/views/common/top.php';?>
<!-- END TOP OF THE PAGE -->

<!-- WRAPPER START -->
<div class="wrapper bg-dark-white">

    <!--   HEADER START   -->
    <?php require_once 'public/views/common/header.php';?>
    <!--   END HEADER   -->
    <!-- HEADING-BANNER START -->
    <?php require_once 'public/views/common/bannerwithbreadcrumbs.php';?>
    <!-- HEADING-BANNER END -->
    <!-- PRODUCT-AREA START -->
    <!-- SHOPPING-CART-AREA START -->
    <script>
        function demoDisplay() {
            document.getElementById("sel").style.display = "none";
        }
    </script>
    <div class="login-area  pt-80 pb-80">
        <div class="container">
            <form action="<?=root()?>/myshop/store" method="post">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="customer-login text-left">
                            <h4 class="title-1 title-border text-uppercase mb-30">Create your own shop</h4>
                            <input type="text" placeholder="Shop name..." name="shop">
                            <select class="custom-select mb-15" name="category" >
                                <option value="">Select category</option>
                                <?php
                                foreach ($data['categories'] as $category) {
                                    echo '<option value="'.$category->id.'">'.$category->name .'</option>';
                                }
                                ?>
                            </select>
                            <button type="submit" name="createShop" data-text="Create shop" class="button-one submit-button btn-bg-3 mt-20">Create shop</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <!-- PRODUCT-AREA END -->
    <!--  START FOOTER -->
    <?php require_once 'public/views/common/footer.php';?>
    <!-- END FOOTER -->

</div>
<!-- WRAPPER END -->

<!--  START BOTTOM OF THE PAGE -->
<?php require_once 'public/views/common/bottom_product.php';?>
<!-- END BOTTOM OF THE PAGE -->


