<!--  START TOP OF THE PAGE -->
<?php require_once 'public/views/common/top_product.php';?>
<!-- END TOP OF THE PAGE -->

<!-- WRAPPER START -->
<div class="wrapper" xmlns="http://www.w3.org/1999/html">

    <!--   HEADER START   -->
    <?php require_once 'public/views/common/header.php';?>
    <!--   END HEADER   -->
    <!-- BREADCRUMB START -->
    <?php require_once 'public/views/common/breadcrumb.php';?>
    <!-- BREADCRUMB END -->
    <!-- PRODUCT-AREA START -->
    <div class="product-area single-pro-area pt-80 pb-80 product-style-2">

        <!-- FORM WIZARD START -->
        <div class="container">
            <br />
            <form action="#" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8" enctype="multipart/form-data">

                <!-- SmartWizard html -->
                <div id="smartwizard">
                    <ul>
                        <li><a href="#step-1"><small>Shop</small></a></li>
                        <li><a href="#step-2"><small>Product</small></a></li>
                        <li><a href="#step-3"><small>Shipping</small></a></li>
                        <li><a href="#step-4"><small>T&C</small></a></li>
                    </ul>

                    <div>
                        <div id="step-1">
                            <h2>Your Shop</h2>
                            <div id="form-step-0" role="form" data-toggle="validator">
                                <fieldset class="form-group">
                                    <legend>Shop</legend>
                                    <div class="form-group">
                                        <label for="shop-name">Shop name: <span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" name="shop-name" id="shop-name" onchange="checkShop(this.value,'<?=root()?>/shop/shopAjax/showShop', this.id)" placeholder="Write your shop name" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="step-2">
                            <h2>Your Product</h2>
                            <div id="form-step-1" role="form" data-toggle="validator">
                                <fieldset class="form-group">
                                    <legend>Product</legend>
                                    <div class="form-group">
                                        <label for="title">Title: <span class="text-danger">*</span> </label>
                                        <input type="text" class="form-control" name="title" id="title" onchange="checkValid(this.value, this.id)" placeholder="Write your product title" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">Quantity: <span class="text-danger">*</span> </label>
                                        <input type="number" class="form-control" name="quantity" id="quantity" onchange="checkValid(this.value, this.id)"  min="0" placeholder="Write your product quantity" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="price">Price: <span class="text-danger">*</span> </label>
                                        <input type="number" class="form-control" step="0.01" name="price" id="price" onchange="checkValid(this.value, this.id)" placeholder="Write your product price (&#x9f3;)" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </fieldset>

                                <fieldset class="form-group">
                                    <legend>Product Image</legend>
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img data-src="holder.js/300x200?bg=rgb(200, 161, 101)&fg=rgba(255, 255, 255, 0.9)&text=Add new image" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <div>
                                                <span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="zmdi zmdi-upload"></i> Select</span><span class="fileinput-exists"><i class="zmdi zmdi-image-alt"></i> Change</span><input class="form-control" type="file" name="image1" id="image1" onchange="imageUpload(this.files[0],'<?=root()?>/shop/shopAjax/storeImage', this.id)"></span>
                                                <a href="#" class="btn btn-danger fileinput-exists pull-right" data-dismiss="fileinput"><i class="zmdi zmdi-delete"></i> Remove</a>
                                            </div>
                                        </div>
<!--                                        <div class="fileinput fileinput-new" data-provides="fileinput">-->
<!--                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
<!--                                                <img data-src="holder.js/300x200?bg=rgb(200, 161, 101)&fg=rgba(255, 255, 255, 0.9)&text=Add new image" alt="...">-->
<!--                                            </div>-->
<!--                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>-->
<!--                                            <div>-->
<!--                                                <span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="zmdi zmdi-upload"></i> Select</span><span class="fileinput-exists"><i class="zmdi zmdi-image-alt"></i> Change</span><input class="form-control" type="file" name="..."></span>-->
<!--                                                <a href="#" class="btn btn-danger fileinput-exists pull-right" data-dismiss="fileinput"><i class="zmdi zmdi-delete"></i> Remove</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="fileinput fileinput-new" data-provides="fileinput">-->
<!--                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
<!--                                                <img data-src="holder.js/300x200?bg=rgb(200, 161, 101)&fg=rgba(255, 255, 255, 0.9)&text=Add new image" alt="...">-->
<!--                                            </div>-->
<!--                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>-->
<!--                                            <div>-->
<!--                                                <span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="zmdi zmdi-upload"></i> Select</span><span class="fileinput-exists"><i class="zmdi zmdi-image-alt"></i> Change</span><input class="form-control" type="file" name="..."></span>-->
<!--                                                <a href="#" class="btn btn-danger fileinput-exists pull-right" data-dismiss="fileinput"><i class="zmdi zmdi-delete"></i> Remove</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="fileinput fileinput-new" data-provides="fileinput">-->
<!--                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
<!--                                                <img data-src="holder.js/300x200?bg=rgb(200, 161, 101)&fg=rgba(255, 255, 255, 0.9)&text=Add new image" alt="...">-->
<!--                                            </div>-->
<!--                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>-->
<!--                                            <div>-->
<!--                                                <span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="zmdi zmdi-upload"></i> Select</span><span class="fileinput-exists"><i class="zmdi zmdi-image-alt"></i> Change</span><input class="form-control" type="file" name="..."></span>-->
<!--                                                <a href="#" class="btn btn-danger fileinput-exists pull-right" data-dismiss="fileinput"><i class="zmdi zmdi-delete"></i> Remove</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="fileinput fileinput-new" data-provides="fileinput">-->
<!--                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">-->
<!--                                                <img data-src="holder.js/300x200?bg=rgb(200, 161, 101)&fg=rgba(255, 255, 255, 0.9)&text=Add new image" alt="...">-->
<!--                                            </div>-->
<!--                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>-->
<!--                                            <div>-->
<!--                                                <span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="zmdi zmdi-upload"></i> Select</span><span class="fileinput-exists"><i class="zmdi zmdi-image-alt"></i> Change</span><input class="form-control" type="file" name="..."></span>-->
<!--                                                <a href="#" class="btn btn-danger fileinput-exists pull-right" data-dismiss="fileinput"><i class="zmdi zmdi-delete"></i> Remove</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <legend>Category</legend>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="name">Category 1: <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="name" id="email" placeholder="Write your name" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="name">Category 2: </label>
                                                <input type="text" class="form-control" name="name" id="email" placeholder="Write your name" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="name">Category 3:</label>
                                                <input type="text" class="form-control" name="name" id="email" placeholder="Write your name" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset class="form-group">
                                <fieldset class="form-group">
                                    <legend>Classification</legend>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Brand Name: </label>
                                                <input type="text" class="form-control" name="brand" id="brand" placeholder="Write your product brand names" data-provide="typeahead">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Brands: </label>
                                                <p>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Estasy</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Gental Park</button>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Size: </label>
                                                <input type="text" class="form-control" name="size" id="size" placeholder="Write your product sizes" data-provide="typeahead">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Sizes: </label>
                                                <p>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> M</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> XL</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> XXL</button>

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Color: </label>
                                                <input type="text" class="form-control" name="color" id="color" placeholder="Write your product colors" data-provide="typeahead">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Colors: </label>
                                                <p>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Red</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Green</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Blue</button>

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Material: </label>
                                                <input type="text" class="form-control" name="material" id="material" placeholder="Write your product materials" data-provide="typeahead">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Materials: </label>
                                                <p>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Cotton</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Silk</button>

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Keyword: </label>
                                                <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Write your product keywords" data-provide="typeahead">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label for="name">Sizes: </label>
                                                <p>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Beautiful</button>
                                                    <button type="button" class="btn btn-default btn-round-xs btn-xs"><i class="zmdi zmdi-close-circle"></i> Homemade</button>

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="step-3">
                            <h2>Your Shipping Policy</h2>
                            <div id="form-step-2" role="form" data-toggle="validator">
                                <fieldset class="form-group">
                                    <legend>Shipping</legend>
                                    <div class="form-group">
                                        <div class="well">
                                            <label><input type="checkbox" value="" data-toggle="collapse" data-target="#demo">Home Delivery</label>
                                            <div id="demo" class="collapse">
                                                <fieldset class="form-group">
                                                    <legend>Policies</legend>
                                                    <div class="form-group">
                                                        <label for="time">Delivery Time: <span class="text-danger">*</span> </label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="time" id="time" value="24" placeholder="Write delivery time" required>
                                                            <span class="input-group-addon">Hours</span>
                                                        </div>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="payment">Payment Method: <span class="text-danger">*</span> </label>
                                                        <select name="payment" id="payment" class="js-example-basic-multiple form-control" multiple="multiple" style="width: 100%">
                                                            <option value="1">Cash on</option>
                                                            <option value="2">Bkash</option>
                                                        </select>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="well">
                                            <label><input type="checkbox" value="" disabled>Home Delivery</label>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="well">
                                            <label><input type="checkbox" value="" disabled>Home Delivery</label>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="step-4" class="">
                            <h2>Our Policy</h2>
                            <div id="form-step-3" role="form" data-toggle="validator">
                                <fieldset class="form-group">
                                    <legend>Terms and Conditions</legend>
                                    <div class="article-segment">
                                        <h1>Purchase of products from Chiltern Valley, On line</h1>
                                        <h4>The descriptions on the website, of the products supplied by Chiltern Valley are necessarily subjective but are intended to give a general description of the products to the best of our knowledge. No such description can ever replace the information gained during a tasting at our cellar shop to which you are welcome 7 days a week.
                                            Chiltern Valley warrants that it has the right to supply all products offered by it on the website.
                                            <br/>
                                            When the stock of any particular vintage runs out, Chiltern Valley reserve the right to substitute another vintage of the same wine.
                                            In all other respects and to the maximum extent permissible, Chiltern Valley make no other warranties or promises about the products, and any implied warranties are excluded.
                                        </h4>
                                    </div>
                                    <div class="article-segment">
                                        <h1>Purchase of products from Chiltern Valley, On line</h1>
                                        <h4>The descriptions on the website, of the products supplied by Chiltern Valley are necessarily subjective but are intended to give a general description of the products to the best of our knowledge. No such description can ever replace the information gained during a tasting at our cellar shop to which you are welcome 7 days a week.
                                            Chiltern Valley warrants that it has the right to supply all products offered by it on the website.
                                            <br/>
                                            When the stock of any particular vintage runs out, Chiltern Valley reserve the right to substitute another vintage of the same wine.
                                            In all other respects and to the maximum extent permissible, Chiltern Valley make no other warranties or promises about the products, and any implied warranties are excluded.
                                        </h4>
                                    </div>
                                    <div class="article-segment">
                                        <h1>Purchase of products from Chiltern Valley, On line</h1>
                                        <h4>The descriptions on the website, of the products supplied by Chiltern Valley are necessarily subjective but are intended to give a general description of the products to the best of our knowledge. No such description can ever replace the information gained during a tasting at our cellar shop to which you are welcome 7 days a week.
                                            Chiltern Valley warrants that it has the right to supply all products offered by it on the website.
                                            <br/>
                                            When the stock of any particular vintage runs out, Chiltern Valley reserve the right to substitute another vintage of the same wine.
                                            In all other respects and to the maximum extent permissible, Chiltern Valley make no other warranties or promises about the products, and any implied warranties are excluded.
                                        </h4>
                                    </div>
                                    <div class="article-segment">
                                        <h1>Purchase of products from Chiltern Valley, On line</h1>
                                        <h4>The descriptions on the website, of the products supplied by Chiltern Valley are necessarily subjective but are intended to give a general description of the products to the best of our knowledge. No such description can ever replace the information gained during a tasting at our cellar shop to which you are welcome 7 days a week.
                                            Chiltern Valley warrants that it has the right to supply all products offered by it on the website.
                                            <br/>
                                            When the stock of any particular vintage runs out, Chiltern Valley reserve the right to substitute another vintage of the same wine.
                                            In all other respects and to the maximum extent permissible, Chiltern Valley make no other warranties or promises about the products, and any implied warranties are excluded.
                                        </h4>
                                    </div>
                                    <div class="article-segment">
                                        <h1>Purchase of products from Chiltern Valley, On line</h1>
                                        <h4>The descriptions on the website, of the products supplied by Chiltern Valley are necessarily subjective but are intended to give a general description of the products to the best of our knowledge. No such description can ever replace the information gained during a tasting at our cellar shop to which you are welcome 7 days a week.
                                            Chiltern Valley warrants that it has the right to supply all products offered by it on the website.
                                            <br/>
                                            When the stock of any particular vintage runs out, Chiltern Valley reserve the right to substitute another vintage of the same wine.
                                            In all other respects and to the maximum extent permissible, Chiltern Valley make no other warranties or promises about the products, and any implied warranties are excluded.
                                        </h4>
                                    </div>
                                    <hr/>
                                    <div class=" pull-right">
                                        <label for="terms">I agree with the T&C </label>
                                        <input type="checkbox" id="terms" data-error="Please accept the Terms and Conditions" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- FORM WIZARD END -->

    </div>
    <!-- PRODUCT-AREA END -->

    <!--  START FOOTER -->
    <?php require_once 'public/views/common/footer.php';?>
    <!-- END FOOTER -->
</div>

<!--  START BOTTOM OF THE PAGE -->
<?php require_once 'public/views/common/bottom_product.php';?>
<!-- END BOTTOM OF THE PAGE -->


<script type="text/javascript">
    $(document).ready(function(){

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
            .addClass('btn btn-info')
            .on('click', function(){
                if( !$(this).hasClass('disabled')){
                    var elmForm = $("#myForm");
                    if(elmForm){
                        elmForm.validator('validate');
                        var elmErr = elmForm.find('.has-error');
                        if(elmErr && elmErr.length > 0){
                            alert('Oops we still have error in the form');
                            return false;
                        }else{
                            alert('Great! we are ready to submit form');
                            elmForm.submit();
                            return false;
                        }
                    }
                }
            });
        var btnCancel = $('<button></button>').text('Cancel')
            .addClass('btn btn-danger')
            .on('click', function(){
                $('#smartwizard').smartWizard("reset");
                $('#myForm').find("input, textarea").val("");
            });



        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'dots',
            transitionEffect:'fade',
            toolbarSettings: {toolbarPosition: 'bottom',
                toolbarExtraButtons: [btnFinish, btnCancel]
            },
            anchorSettings: {
                markDoneStep: true, // add done css
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
            }
        });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#form-step-" + stepNumber);
            // stepDirection === 'forward' :- this condition allows to do the form validation
            // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            if(stepDirection === 'forward' && elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.children('.has-error');
                if(elmErr && elmErr.length > 0){
                    // Form validation failed
                    return false;
                }
            }
            return true;
        });

        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            // Enable finish button only on last step
            if(stepNumber == 3){
                $('.btn-finish').removeClass('disabled');
            }else{
                $('.btn-finish').addClass('disabled');
            }
        });

    });
</script>
<script>
    $('.fileinput').fileinput()
</script>
<script type="text/javascript">
    $(".js-example-basic-multiple").select2({
        placeholder: "Select payment method",
    });
</script>
