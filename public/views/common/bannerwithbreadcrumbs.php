
<div class="heading-banner-area overlay-bg" style='background: rgba(0, 0, 0, 0) url("<?=root()?>/public/images/shops/cover/default.jpg") no-repeat scroll center center / cover ;'>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-banner">
                    <div class="heading-banner-title">
                        <h2>SHOP</h2>
                    </div>
                    <div class="breadcumbs pb-15">
                        <ul>
                            <?php if(isset($_GET['url'])): ?>
                                <li><a href="<?=root()?>/">Home</a></li>
                                <?php
                                $breadcrumbUrls = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
                                $subURL = '';
                                for ($i = 0; $i < sizeof($breadcrumbUrls) - 1; $i++): ?>
                                    <?php $subURL .= $breadcrumbUrls[$i] . '/' ?>
                                    <li><a href="<?=root()?>/<?=$subURL?>"><?=$breadcrumbUrls[$i]?></a></li>
                                <?php endfor; ?>
                                <li><?=$breadcrumbUrls[$i]?></li>
                            <?php else: ?>
                                <li>Home</li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

