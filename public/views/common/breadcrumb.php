<div class="container">
    <div class="breadcumbs pb-15">
        <ul>
            <?php if(isset($_GET['url'])): ?>
                <li><a href="<?=root()?>/">Home</a></li>
                <?php
                $breadcrumbUrls = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
                $subURL = '';
                for ($i = 0; $i < sizeof($breadcrumbUrls) - 1; $i++): ?>
                    <?php $subURL .= $breadcrumbUrls[$i] . '/' ?>
                    <li><a href="<?=root()?>/<?=$subURL?>"><?=$breadcrumbUrls[$i]?></a></li>
                <?php endfor; ?>
                <li><?=$breadcrumbUrls[$i]?></li>
            <?php else: ?>
                <li>Home</li>
            <?php endif; ?>
        </ul>
    </div>
</div>