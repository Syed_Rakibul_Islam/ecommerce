<!-- Mobile-header-top Start -->
<div class="mobile-header-top hidden-lg hidden-md hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- header-search-mobile start -->
                <div class="header-search-mobile">
                    <div class="table">
                        <div class="table-cell">
                            <ul>
                                <li><a class="search-open" href="#"><i class="zmdi zmdi-search"></i></a></li>
                                <li><a href="login.html"><i class="zmdi zmdi-lock"></i></a></li>
                                <li><a href="my-account.html"><i class="zmdi zmdi-account"></i></a></li>
                                <li><a href="wishlist.html"><i class="zmdi zmdi-favorite"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- header-search-mobile start -->
            </div>
        </div>
    </div>
</div>
<!-- Mobile-header-top End -->
<!-- HEADER-AREA START -->
<header id="sticky-menu" class="header header-2">
    <div class="header-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 col-xs-7">
                    <div class="logo text-center">
                        <a href="<?=root()?>"><img src="<?=root()?>/public/user/images/logo.png " alt="" /></a>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-5">
                    <div class="mini-cart text-right">
                        <ul>
                            <li>
                                <a class="cart-icon" href="#">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                    <span>3</span>
                                </a>
                                <div class="mini-cart-brief text-left">
                                    <div class="cart-items">
                                        <p class="mb-0">You have <span>03 items</span> in your shopping bag</p>
                                    </div>
                                    <div class="all-cart-product clearfix">
                                        <div class="single-cart clearfix">
                                            <div class="cart-photo">
                                                <a href="#"><img src="<?=root()?>/public/user/img/cart/1.jpg" alt="" /></a>
                                            </div>
                                            <div class="cart-info">
                                                <h5><a href="#">dummy product name</a></h5>
                                                <p class="mb-0">Price : $ 100.00</p>
                                                <p class="mb-0">Qty : 02 </p>
                                                <span class="cart-delete"><a href="#"><i class="zmdi zmdi-close"></i></a></span>
                                            </div>
                                        </div>
                                        <div class="single-cart clearfix">
                                            <div class="cart-photo">
                                                <a href="#"><img src="<?=root()?>/public/user/img/cart/2.jpg" alt="" /></a>
                                            </div>
                                            <div class="cart-info">
                                                <h5><a href="#">dummy product name</a></h5>
                                                <p class="mb-0">Price : $ 300.00</p>
                                                <p class="mb-0">Qty : 01 </p>
                                                <span class="cart-delete"><a href="#"><i class="zmdi zmdi-close"></i></a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-totals">
                                        <h5 class="mb-0">Total <span class="floatright">$500.00</span></h5>
                                    </div>
                                    <div class="cart-bottom  clearfix">
                                        <a href="#" class="button-one floatleft text-uppercase" data-text="View cart">View cart</a>
                                        <a href="#" class="button-one floatright text-uppercase" data-text="Check out">Check out</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MAIN-MENU START -->
    <div class="menu-toggle hamburger hamburger--emphatic hidden-xs">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div class="main-menu  hidden-xs">
        <nav>
            <ul>
                <li><a href="<?=root()?>/">Home </a></li>
                <?php foreach($this->categories2 as $item): ?>
                    <li><a href="shop.html"><?=$item->name;?></a>
                        <div class="mega-menu menu-scroll">
                            <div class="table">
                                <div class="table-cell">
                                    <div class="half-width">
                                        <ul>
                                            <li class="menu-title"><?=$item->name;?>'s Categories</li>
                                            <?php foreach ($item->category_3 as $item2):?>
                                            <li><a href="#"><?=$item2->name?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <div class="half-width">
                                        <ul>
                                            <li class="menu-title">popular brands</li>
                                            <li><a href="#">Ecstasy</a></li>
                                        </ul>
                                    </div>
                                    <div class="full-width">
                                        <div class="mega-menu-img">
                                            <a href="single-product.html"><img src="<?=root()?>/public/user/img/megamenu/1.jpg" alt="" /></a>
                                        </div>
                                    </div>
                                    <div class="pb-80"></div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
<!--                <li><a href="shop-sidebar.html">new items</a></li>-->
<!--                <li><a href="blog.html">blog</a></li>-->
                <li><a href="<?=root();?>/myshop/policy">Shop</a></li>
                <li><a href="about.html">about us</a></li>
                <li><a href="contact.html">contact</a></li>
            </ul>
        </nav>
    </div>
    <!-- MAIN-MENU END -->
</header>
<!-- HEADER-AREA END -->
<!-- Mobile-menu start -->
<div class="mobile-menu-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 hidden-lg hidden-md hidden-sm">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul>
                            <li><a href="<?=root()?>/">Home</a></li>
                            <li><a href="shop.html">products</a></li>
                            <li><a href="shop-sidebar.html">accesories</a></li>
                            <li><a href="shop-list.html">lookbook</a></li>
                            <li><a href="blog.html">blog</a></li>
                            <li><a href="#">pages</a>
                                <ul>
                                    <li><a href="shop.html">Shop</a></li>
                                    <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
                                    <li><a href="shop-list.html">Shop List</a></li>
                                    <li><a href="single-product.html">Single Product</a></li>
                                    <li><a href="single-product-sidebar.html">Single Product Sidebar</a></li>
                                    <li><a href="cart.html">Shopping Cart</a></li>
                                    <li><a href="wishlist.html">Wishlist</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="order.html">Order</a></li>
                                    <li><a href="login.html">login / Registration</a></li>
                                    <li><a href="my-account.html">My Account</a></li>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="blog.html">Blog</a></li>
                                    <li><a href="single-blog.html">Single Blog</a></li>
                                    <li><a href="single-blog-sidebar.html">Single Blog Sidebar</a></li>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </li>
                            <li><a href="about.html">about us</a></li>
                            <li><a href="contact.html">contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Mobile-menu end -->



<?php if(isset($data['message']['type'])):?>
    <div class="alert alert-<?=$data['message']['type']?>" id="session-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><?=$data['message']['title']?>!</strong>
        <?php if(is_array($data['message']['message'])): ?>
            <ul class="list-group">
                <?php foreach ($data['message']['message'] as $message): ?>
                    <li class="list-group-item"><?php print_r($message);?></li>
                <?php endforeach;?>
            </ul>
        <?php else: ?>
            <?= $data['message']['message']; ?>
        <?php endif; ?>
    </div>
<?php endif;?>