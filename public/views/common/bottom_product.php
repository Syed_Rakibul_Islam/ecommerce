<!-- all js here -->
<!-- jquery latest version -->
<script src="<?=root()?>/public/user/js/vendor/jquery-1.12.0.min.js"></script>
<!-- bootstrap js -->
<script src="<?=root()?>/public/user/js/bootstrap.min.js"></script>
<!-- jquery.meanmenu js -->
<script src="<?=root()?>/public/user/js/jquery.meanmenu.js"></script>
<!-- slick.min js -->
<script src="<?=root()?>/public/user/js/slick.min.js"></script>
<!-- jquery.treeview js -->
<script src="<?=root()?>/public/user/js/jquery.treeview.js"></script>
<!-- lightbox.min js -->
<!--<script src="--><?//=root()?><!--/public/user/js/lightbox.min.js"></script>-->
<!-- jquery-ui js -->
<script src="<?=root()?>/public/user/js/jquery-ui.min.js"></script>
<!-- jquery.nivo.slider js -->
<script src="<?=root()?>/public/user/lib/js/jquery.nivo.slider.js"></script>
<script src="<?=root()?>/public/user/lib/home.js"></script>
<!-- jquery.nicescroll.min js -->
<script src="<?=root()?>/public/user/js/jquery.nicescroll.min.js"></script>
<!-- countdon.min js -->
<script src="<?=root()?>/public/user/js/countdon.min.js"></script>
<!-- wow js -->
<script src="<?=root()?>/public/user/js/wow.min.js"></script>
<!-- plugins js -->
<script src="<?=root()?>/public/user/js/plugins.js"></script>
<script src="<?=root()?>/public/user/js/notify.min.js"></script>
<!-- main js -->
<script src="<?=root()?>/public/user/js/main.js"></script>

<script src="<?=root()?>/public/user/js/custom.js"></script>


<!-- Include jQuery Validator plugin -->
<script src="<?=root()?>/public/plugins/wizard/js/validator.min.js"></script>

<!-- BOOTSTRAP SELECT -->
<script  src="<?=root()?>/public/plugins/select2/dist/js/select2.min.js"></script>

<!-- Include SmartWizard JavaScript source -->
<script type="text/javascript" src="<?=root()?>/public/plugins/wizard/js/jquery.smartWizard.min.js"></script>


<!--DROPZONE IMAGE UPLOADER-->
<script src="<?=root()?>/public/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
<script src="<?=root()?>/public/plugins/holder/holder.min.js"></script>
<script src="<?=root()?>/public/user/js/product.js"></script>


<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>