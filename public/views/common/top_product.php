<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>E-commerce</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="<?=root()?>/public/user/img/icon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>

    <!-- all css here -->

    <!-- Initialize css -->
    <link rel="stylesheet" href="<?=root()?>/public/css/init.css">

    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- animate css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/animate.css">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/jquery-ui.min.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/meanmenu.min.css">
    <!-- nivo-slider css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/lib/css/nivo-slider.css">
    <link rel="stylesheet" href="<?=root()?>/public/user/lib/css/preview.css">
    <!-- slick css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/slick.css">
    <!-- lightbox css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/lightbox.min.css">
    <!-- material-design-iconic-font css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/material-design-iconic-font.css">
    <!-- All common css of theme -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/default.css">
    <!-- style css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/style.css">
    <!-- shortcode css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/shortcode.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?=root()?>/public/user/css/responsive.css">

    <!--   SELECT2  -->
    <link href="<?=root()?>/public/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!--    WIZARD FORM  DESIGN -->
    <!-- Include SmartWizard CSS -->
    <link href="<?=root()?>/public/plugins/wizard/css/smart_wizard.css" rel="stylesheet" type="text/css" />

    <!-- Optional SmartWizard theme -->
    <link href="<?=root()?>/public/plugins/wizard/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

    <!--  DROPZONE IMAGEUPLOADER  -->
    <link href="<?=root()?>/public/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css" />


    <link href="<?=root()?>/public/user/css/product.css" rel="stylesheet" type="text/css" />

    <!-- modernizr css -->
    <script src="<?=root()?>/public/user/js/vendor/modernizr-2.8.3.min.js"></script>

</head>
<body>
